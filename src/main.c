#include "stm32f10x.h"
#include "stm32f1xx_nucleo.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#define CLEAR(x) memset(x,'\0',sizeof(x))

char readBuffer[256];
char writeBuffer[256];
char temp[128];
char name[128];

char* addr;
char* value;
char* ch;

char cmdBuffer[256];
short cmdB = 0;

char startBuffer[256];
short cmdA = 0;
short isStarted = 0;
short isDone = 0;

short rHeadIndex = 0;
short rTailIndex = 0;
short wHeadIndex = 0;
short wTailIndex = 0;

void RCC_Config(void)
{
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOB | RCC_APB2Periph_GPIOC, ENABLE);
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_SPI1 , ENABLE);
}


void GPIO_Config(void)
{
  GPIO_InitTypeDef  GPIO_InitStructure;

  // SPI - CS
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init(GPIOA, &GPIO_InitStructure);

  //SPI - SCK, MISO, MOSI
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_5 | GPIO_Pin_6 | GPIO_Pin_7;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init(GPIOA, &GPIO_InitStructure);

  //USART2 - TX
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
  GPIO_Init(GPIOA, &GPIO_InitStructure);

  //USART2 - RX
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
  GPIO_Init(GPIOA, &GPIO_InitStructure);
}

void USART2_Config(void)
{
	USART_InitTypeDef usart;
	USART_StructInit(&usart);

	usart.USART_BaudRate = 9600;
	usart.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	usart.USART_Parity = USART_Parity_No;
	usart.USART_WordLength = USART_WordLength_8b;
	usart.USART_StopBits = USART_StopBits_1;

	USART_Init(USART2, &usart);
	USART_Cmd(USART2, ENABLE);

	IRQn_Type UT2_IRQn = 38;
	NVIC_EnableIRQ(UT2_IRQn);

	USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);
}


void SPI_Config(void)
{
  SPI_InitTypeDef   SPI_InitStructure;

  SPI_InitStructure.SPI_Direction =  SPI_Direction_2Lines_FullDuplex;
  SPI_InitStructure.SPI_Mode = SPI_Mode_Master;
  SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b ;
  SPI_InitStructure.SPI_CPOL = SPI_CPOL_High;
  SPI_InitStructure.SPI_CPHA = SPI_CPHA_2Edge;
  SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;
  SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_4;
  SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;
  SPI_InitStructure.SPI_CRCPolynomial = 7;
  SPI_Init(SPI1, &SPI_InitStructure);

  SPI_Cmd(SPI1, ENABLE);
}

void USART2_IRQHandler(void){

	if(USART_GetITStatus(USART2, USART_IT_RXNE) != RESET){
		char c = USART_ReceiveData(USART2);
		readBuffer[rHeadIndex++] = c;

		if(rHeadIndex == 256){
			rHeadIndex = 0;
		}
	}
	if(USART_GetITStatus(USART2, USART_IT_TXE) != RESET){

		if(wHeadIndex != wTailIndex) {
					USART_SendData(USART2, writeBuffer[wTailIndex++]);

					if(wTailIndex == 256) {
						wTailIndex = 0;
					}
				}
				else {
					USART_ITConfig(USART2, USART_IT_TXE, DISABLE);
				}
			}
		}

	uint8_t spi_sendrecv(uint8_t byte)
		{
		 // poczekaj az bufor nadawczy bedzie wolny
		 while (SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_TXE) == RESET);
		 SPI_I2S_SendData(SPI1, byte);

		 // poczekaj na dane w buforze odbiorczym
		 while (SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_RXNE) == RESET);
		 return SPI_I2S_ReceiveData(SPI1);
		}

	void mcp_write_reg(uint8_t addr, uint8_t value)
	{
		GPIO_ResetBits(GPIOA, GPIO_Pin_4);
		 spi_sendrecv(addr);
		 spi_sendrecv(value);
		 GPIO_SetBits(GPIOA, GPIO_Pin_4);
	}


		uint8_t mcp_read_reg(uint8_t addr)
		{
		 GPIO_ResetBits(GPIOA, GPIO_Pin_4);
		 spi_sendrecv(addr);
		 uint8_t value = spi_sendrecv(0xff);
		 GPIO_SetBits(GPIOA, GPIO_Pin_4);
		 return value;
		}



		void sendString(char * string) {
			short id = wHeadIndex;

			while(*string) {
				writeBuffer[id++] = *string++;

				if(id == 256) {
					id = 0;
				}
			}

			__disable_irq();

			if((wHeadIndex == wTailIndex) && (USART_GetFlagStatus(USART2, USART_FLAG_TXE) == SET)){
				wHeadIndex = id;
				USART_SendData(USART2, writeBuffer[wTailIndex++]);

				if(wTailIndex == 256) {
					wTailIndex = 0;
				}

				USART_ITConfig(USART2, USART_IT_TXE, ENABLE);
			} else {
				wHeadIndex = id;
			}

			__enable_irq();
		}

		int porownaj_dl(const char*a, const char*b, uint8_t dlugosc){
			int i = 0;
			for (; i < dlugosc; i++) {
				if (a[i] != b[i]) {
					return 0;
				}
			} return 1;
		}



		void porownaj(char *tablica, uint8_t dlugosc){
			uint8_t start;
			uint8_t i, response;
			int counter = 0;

			if (dlugosc >= 9 && porownaj_dl(tablica, "write reg", 9) == 1) {
				start = 10;
				i = dlugosc - start;
				strncpy(name, tablica+start, i);
				name[i]=0x00;
				ch = strtok(name, " ");
				while (ch != NULL) {
					if (counter==0) {
						addr=ch;
					} else if (counter==1) {
						value=ch;
					}
					ch = strtok(NULL, " ");
					counter++;
				}
				if (!*value) {
					sendString("Brak drugiego parametru\r\n");
				} else {
					mcp_write_reg((uint8_t *)addr, (uint8_t *)value);
					sprintf(temp, "#;write reg %s %s#END;\r\n", addr, value);
					sendString(temp);
				}

			}else if (dlugosc >= 8 && porownaj_dl(tablica, "read reg", 8) == 1) {
				start = 9;
				i = dlugosc - start;
				strncpy(name, tablica+start, i);
				name[i]=0x00;
				response = mcp_read_reg((uint8_t)name);
				sprintf(temp, "#;read reg 0x%X#END;\r\n", response);
				sendString(temp);
			}else{
				sendString("Dostepne komendy '#;read reg adres#END;', '#;write reg adres wartosc#END;' \r\n");
			}
			CLEAR(temp);
			response=0;
			CLEAR(value);
			CLEAR(addr);
		}

		int main(void)
		{
			RCC_Config();
			SPI_Config();
			GPIO_Config();
			USART2_Config();

		 while(1){
			 while (rTailIndex != rHeadIndex){

			 						cmdBuffer[cmdB] = readBuffer[rTailIndex++];

			 						if (rTailIndex >= 256){
			 							rTailIndex = 0;
			 						}

			 						if (isStarted == 2) {
			 							startBuffer[cmdA] = cmdBuffer[cmdB];
			 							cmdA++;
			 						}

			 						if (cmdBuffer[cmdB] == '#' && isStarted == 0){
			 							isStarted = 1;
			 						}

			 						if (cmdBuffer[cmdB] == ';' && isStarted == 1) {
			 							isStarted = 2;
			 						}

			 						if (cmdBuffer[cmdB] == '#' && isStarted == 2) {
			 							isDone = 1;
			 						}

			 						if (cmdBuffer[cmdB] == 'E' && isStarted == 2 && isDone == 1) {
			 							isDone = 2;
			 						}

			 						if (cmdBuffer[cmdB] == 'N' && isStarted == 2 && isDone == 2) {
			 							isDone = 3;
			 						}

			 						if (cmdBuffer[cmdB] == 'D' && isStarted == 2 && isDone == 3) {
			 							isDone = 4;
			 						}

			 						if (cmdBuffer[cmdB] == ';' && isStarted == 2 && isDone == 4) {
			 							porownaj(startBuffer, cmdA-5);
			 							cmdA = 0;
			 							isStarted = 0;
			 							isDone = 0;
			 						}

			 						cmdB++;
			 				}
		 	 	 }
		}
